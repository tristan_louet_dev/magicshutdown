package main

import (
	"flag"
	"os"
	"log"
	"magicshutdown"
	"strconv"
	"fmt"
	"net"
)

const (
	DEFAULT_LOG_FILE_PATH = "log.txt"
	DEFAULT_SHUTDOWN_DELAY = 10
	DEFAULT_VERBOSE = false
	DEFAULT_UDP_PORT = 42042

	MAGIC_PACKET_PATTERN_REPEAT_NB = 64
)

const (
	MAGIC_PACKET_PATTERN byte = 42
)

var verboseServer *bool
var logFile *os.File

func main() {
	//Command line arguments
	logFilePath := flag.String("log", DEFAULT_LOG_FILE_PATH, "Path to the program log file. Default is \"" + DEFAULT_LOG_FILE_PATH + "\", in the execution folder.")
	timeBeforeShutdown := flag.Int("delay", DEFAULT_SHUTDOWN_DELAY, "The time in seconds before the computer shuts down. Default is " + strconv.FormatInt(int64(DEFAULT_SHUTDOWN_DELAY), 10) + ".")
	verboseServer = flag.Bool("verbose", DEFAULT_VERBOSE, "Does the server log every possible action or not. Default is " + strconv.FormatBool(DEFAULT_VERBOSE) + ".")
	udpPort := flag.Int("port", DEFAULT_UDP_PORT, "The port for the UDP socket to listen to. Default is " + strconv.FormatInt(int64(DEFAULT_UDP_PORT), 10) + ".")

	flag.Parse()
	//----------------------
	logMessage("Verbose mode requested: the server will talk a lot", false)
	logMessage("Delay before shutdown set to " + strconv.FormatInt(int64(*timeBeforeShutdown), 10), false)
	logMessage("Opening log file " + *logFilePath, false)
	var fileError error
	logFile, fileError = os.OpenFile(*logFilePath, os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	if fileError != nil {
		logMessage("Failed to open log file", true)
		log.Fatal(fileError)
	}
	defer logFile.Close()

	logMessage("Resolving UDP address", false)
	var strUdpPort = strconv.FormatInt(int64(*udpPort), 10)
	addr, err := net.ResolveUDPAddr("udp", ":" + strUdpPort)
	if err != nil {
		logMessage("Failed to resolve UDP address", true)
		logFile.WriteString("Failed resolve UDP address")
		log.Fatal(err)
	}
	logMessage("Starting up UDP server on port " + strUdpPort, false)
	socket, err := net.ListenUDP("udp", addr)
	if err != nil {
		logMessage("Failed to start UDP server", true)
		logFile.WriteString("Failed to start UDP server")
		log.Fatal(err)
	}

	logMessage("UDP server now listening on port " + strUdpPort + "...", false)
	for {
		handleRequest(socket, *timeBeforeShutdown)
	}
}

func logMessage(message string, bypassVerboseLevel bool) {
	if !bypassVerboseLevel && !*verboseServer {
		return
	}
	fmt.Println("Log: " + message)
}

func handleRequest(socket *net.UDPConn, shutdownDelay int) {
	buffer := make([]byte, MAGIC_PACKET_PATTERN_REPEAT_NB)
	len, _, err := socket.ReadFromUDP(buffer)

	logMessage("Processing packet data...", false)
	if err != nil {
		logMessage("An error occured while reading from the UDP socket", true)
		logFile.WriteString("An error occured while reading from the UDP socket")
		log.Fatal(err)
	}

	if len != MAGIC_PACKET_PATTERN_REPEAT_NB {
		//Someones sent a non-magic packet
		logMessage("UDP packet of length " + strconv.FormatInt(int64(len), 10) + " ignored", false)
		return
	}
	//Vérifier si le pattern est bien un magic packet
	for pos, val := range buffer {
		if val != MAGIC_PACKET_PATTERN {
			logMessage("UDP packet of length " + strconv.FormatInt(int64(len), 10) + " ignored because of incorrect value at position " + strconv.FormatInt(int64(pos), 10), false)
			return
		}
	}

	requestShutdown(shutdownDelay)
}

func requestShutdown(delay int) {
	logMessage("Requesting shutdown with " + strconv.FormatInt(int64(delay), 10) + " seconds delay", false)
	err := magicshutdown.Shutdown(delay)
	if err != nil {
		logMessage("Failed to shutdown computer", true)
		logFile.WriteString("Failed to shutdown computer")
		log.Fatal(err)
	}
	logMessage("Shutdown request successfully registered", false)
}
