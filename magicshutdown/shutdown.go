package magicshutdown

import (
	"os/exec"
	"strconv"
)

func Shutdown (delay int) error {
	command := exec.Command("cmd.exe", "/c", "shutdown.exe", "/s", "/t", strconv.FormatInt(int64(delay), 10))
	return command.Run()
}

func InterruptShutdown () error {
	command := exec.Command("cmd.exe", "/c", "shutdown.exe", "/a")
	return command.Run()
}
